import { ShopifyProductResponse } from "../shopify/shopify.product.model"

export enum ProductStatus {
  ACTIVE = 'active',
  DRAFT = 'draft',
  ARCHIVED = 'archived',
}
interface ProductImage {
  id: number
  productId: number
  position: number
  createdAt: string
  updatedAt: string
  width: number
  height: number
  src: string
  variantIds: number[]
}
interface ProductVariant {
  id: number
  productId: number
  title: string
  price: string
  sku: string | null
  position: number
  inventoryPolicy: string
  fulfillmentService: string
  inventoryManagement: string | null
  option1: string | null
  option2: string | null
  option3: string | null
  createdAt: string
  updatedAt: string
  barcode: string | null
  weight: number
  weight_unit: string
  inventoryItemId: number
  inventoryQuantity: number
  imageId: number | null
}

export class ProductResponse {
  id: number
  title: string
  bodyHtml: string
  createdAt: string
  status: ProductStatus
  images: ProductImage[]
  image: ProductImage | null
  productType: string
  publishedAt: string
  variants: ProductVariant[]

  constructor(data: ShopifyProductResponse) {
    this.id = data.id
    this.title = data.title
    this.bodyHtml = data.body_html
    this.createdAt = data.created_at
    this.status = data.status as ProductStatus
    this.images = data.images.map((image) => ({
      id: image.id,
      productId: image.product_id,
      position: image.position,
      createdAt: image.created_at,
      updatedAt: image.updated_at,
      width: image.width,
      height: image.height,
      src: image.src,
      variantIds: image.variant_ids,
    }))
    this.image = data.image
      ? {
          id: data.image.id,
          productId: data.image.product_id,
          position: data.image.position,
          createdAt: data.image.created_at,
          updatedAt: data.image.updated_at,
          width: data.image.width,
          height: data.image.height,
          src: data.image.src,
          variantIds: data.image.variant_ids,
        }
      : null
    this.productType = data.product_type
    this.publishedAt = data.published_at
    this.variants = data.variants.map((variant) => ({
      id: variant.id,
      productId: variant.product_id,
      title: variant.title,
      price: variant.price,
      sku: variant.sku,
      position: variant.position,
      inventoryPolicy: variant.inventory_policy,
      fulfillmentService: variant.fulfillment_service,
      inventoryManagement: variant.inventory_management,
      option1: variant.option1,
      option2: variant.option2,
      option3: variant.option3,
      createdAt: variant.created_at,
      updatedAt: variant.updated_at,
      barcode: variant.barcode,
      weight: variant.weight,
      weight_unit: variant.weight_unit,
      inventoryItemId: variant.inventory_item_id,
      inventoryQuantity: variant.inventory_quantity,
      imageId: variant.image_id,
    }))
  }
}
