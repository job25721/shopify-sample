export interface ShopifyProductImageResponse {
  id: number
  product_id: number
  position: number
  created_at: string
  updated_at: string
  width: number
  height: number
  src: string
  variant_ids: number[]
}

export interface ShopifyProductVariantResponse {
  id: number
  product_id: number
  title: string
  price: string
  sku: string | null
  position: number
  inventory_policy: string
  fulfillment_service: string
  inventory_management: string | null
  option1: string | null
  option2: string | null
  option3: string | null
  created_at: string
  updated_at: string
  barcode: string | null
  weight: number
  weight_unit: string
  inventory_item_id: number
  inventory_quantity: number
  image_id: number
}

export interface ShopifyProductResponse {
  id: number
  title: string
  body_html: string
  created_at: string
  status: string
  images: ShopifyProductImageResponse[]
  image: ShopifyProductImageResponse | null
  product_type: string
  published_at: string
  variants: ShopifyProductVariantResponse[]
}
