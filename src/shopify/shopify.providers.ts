import '@shopify/shopify-api/adapters/node'
import dotenv from 'dotenv'

import { shopifyApi, LATEST_API_VERSION } from '@shopify/shopify-api'
import { ShopifyProductResponse } from './shopify.product.model'
import { RestClient } from '@shopify/shopify-api/lib/clients/admin/rest/client'

dotenv.config()

const shopify = shopifyApi({
  apiKey: process.env.SHOPIFY_API_KEY,
  apiSecretKey: process.env.SHOPIFY_API_SECRET_KEY || '',
  adminApiAccessToken: process.env.SHOPIFY_API_SECRET_KEY,
  hostName: 'localhost:8080',
  apiVersion: LATEST_API_VERSION,
  isEmbeddedApp: false,
  isCustomStoreApp: true,
})

export class ShopifyProvider {
  private client: RestClient

  constructor() {
    const session = shopify.session.customAppSession(process.env.SHOPIFY_SHOP_NAME || '')
    if (!session) {
      throw new Error('Session not found')
    }

    this.client = new shopify.clients.Rest({ session })
  }

  async getAllProducts(): Promise<ShopifyProductResponse[]> {
    const response = await this.client.get<{ products: ShopifyProductResponse[] }>({
      path: 'products',
    })
    return response.body.products
  }
}
