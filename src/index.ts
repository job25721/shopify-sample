import express from 'express'
import { ShopifyProvider } from './shopify/shopify.providers'
import { ProductResponse } from './models/product.model'

const app = express()

const port = process.env.PORT || 8080

const shopifyProvider = new ShopifyProvider()

app.get('/products', async (req, res) => {
  const response = await shopifyProvider.getAllProducts()
  const products = response.map((product) => new ProductResponse(product))
  res.json(products)
})

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`)
})
